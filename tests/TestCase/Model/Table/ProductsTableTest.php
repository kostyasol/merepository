<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductsTable Test Case
 */
class ProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductsTable
     */
    public $Products;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.products',
        'app.users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Products') ? [] : ['className' => 'App\Model\Table\ProductsTable'];
        $this->Products = TableRegistry::get('Products', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Products);

        parent::tearDown();
    }

    /**
     * Test isAdmin method
     *
     * @return void
     */
    public function testIsAdmin()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test generateListStack method
     *
     * @return void
     */
    public function testGenerateListStack()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test addInbasketProduct method
     *
     * @return void
     */
    public function testAddInbasketProduct()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test deleteFromBasketProduct method
     *
     * @return void
     */
    public function testDeleteFromBasketProduct()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getProductById method
     *
     * @return void
     */
    public function testGetProductById()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test getAllCountForBasket method
     *
     * @return void
     */
    public function testGetAllCountForBasket()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test deleteOneInProdForId method
     *
     * @return void
     */
    public function testDeleteOneInProdForId()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buyIfCan method
     *
     * @return void
     */
    public function testBuyIfCan()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
