<?php

namespace App\Model\Table;

use App\Model\Entity\Product;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table {

    var $us;

    public function isAdmin($userId) {
        if ($userId == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function generateListStackProductsForAuth($products) { //ganarate list products with amount (stack list) of products in database
        if (isset($products)) {
            $productsNonRepit = array();
            foreach ($products as $currentProduct) {
                $productsNonRepit+=[ $currentProduct->id => $currentProduct->_joinData->amount];
            }

            return $productsNonRepit;
        }
        return 'null';
    }

    public function generateListStackProductsForNotAuth($products) {// ganarate list products with amount (stack list) of products entity in session
        if (isset($products)) {
            $productsNonRepit = array();
            foreach ($products as $currentProduct) {
                $curProdId = $currentProduct->id;
                if (isset($productsNonRepit[$curProdId]) == false) {
                    $productsNonRepit+= [$curProdId => 1];
                } else {
                    $productsNonRepit[$curProdId] = $productsNonRepit[$curProdId] + 1;
                }
            }

            return $productsNonRepit;
        }
        return 'null';
    }

    public function addInDataBaseBasketOrder($products, $user) {//add new order in database and add products
        
        $order = $this->Orders->newEntity();
        $order->is_paid = 0;
        $order->user_id = $user['id'];
        $order->products = array($products);
        $this->Orders->save($order);
    }

    public function updateProductInOrderIfCan($product, $order) { // 
        for ($i = 0; $i < count($order->products); $i++) {
            if ($order->products[$i]->id == $product->id) {
                $order->products[$i]->_joinData->amount = ($order->products[$i]->_joinData->amount + 1);
                $this->Orders->OrdersProducts->save($order->products[$i]->_joinData);
                return true;
            }
        }
        return false;
    }
    

    public function addNewProductInOrder($product, $order) { // 
        $new_prods = array();
        for ($i = 0; $i < count($order->products); $i++) {
            array_push($new_prods, $order->products[$i]);
        }
         array_push($new_prods, $product);
         
         $order->products = $new_prods;
        $this->Orders->save($order);
    }
    
    
    public function mergeInbasketDBProduct($product, $order) {
        $new_prods = array();
        if(!$this->updateProductInOrderIfCan($product, $order)){
            $this->addNewProductInOrder($product, $order);
        }
    }

    public function getOrderForThisUserNotPaid($user) {
        $query = $this->Orders->find(
                        'all', ['contain' => ['Users', 'Products']])
                ->where(['user_id' => $user['id']])
                ->where(['is_paid' => 0]);

        return $query->first();
    }

    public function addInBasketProductIsAuth($product, $user) {
        $order = $this->getOrderForThisUserNotPaid($user);

        if ($order) {
            $this->mergeInbasketDBProduct($product, $order);
        } else {
            $this->addInDataBaseBasketOrder($product, $user);
        }
    }

    public function addInBasketProductIsNotAuth($product, $session) {

        if ($session->check('prod')) {
            $session->write('prod', array_merge($session->read('prod'), array($product)));
            return $session->read('prod');
        } else {
            $session->write('prod', array($product));
        }
    }

    public function deleteFromBasketProductIsAuth($id, $user) {
        $order = $this->getOrderForThisUserNotPaid($user);
        
        for ($i = 0; $i < count($order->products); $i++) {
            if ($order->products[$i]->id == $id) {
                if ($order->products[$i]->_joinData->amount > 1) {
                    $order->products[$i]->_joinData->amount = ($order->products[$i]->_joinData->amount - 1);
                    $this->Orders->OrdersProducts->save($order->products[$i]->_joinData);
                    return NULL;
                } else {
                    $this->Orders->OrdersProducts->delete($order->products[$i]->_joinData);
                    $this->Orders->OrdersProducts->save($order->products[$i]->_joinData);
                    return NULL;
                }
            }
        }
    }

    public function deleteFromBasketProductIsNotAuth($id, $session) {
        if ($session->check('prod')) {
            $sessionProdBuff = $session->read('prod');
            $resultThenDelete = $this->deleteOneInProdForId($sessionProdBuff, $id);
            $session->write('prod', $resultThenDelete);
        }
    }

    public function getProductById($id) {
        return $this->get($id, ['contain' => []]);
    }

    public function getSumCountProductsInBasketIsAuth($user) {
        $allCount = 0;
        $order = $this->getOrderForThisUserNotPaid($user);

        if ($order) {
            $products = $order->products;
            foreach ($products as $oneProduct) {
                $allCount+= ($oneProduct->price * $oneProduct->_joinData->amount);
            }
            return $allCount;
        }
        return 0;
    }

    public function deleteOneInProdForId($arrayProducts, $idProduct) {
        foreach ($arrayProducts as $keyForUnset => $currentProduct) {
            if (isset($currentProduct->id)) {
                if ($currentProduct->id == $idProduct) {
                    unset($arrayProducts[$keyForUnset]);
                    return $arrayProducts;
                }
            }
        }
        return $arrayProducts;
    }

    public function getProductsIds($session) {
        $products = $session->read('prod');
        $productsIds = array();
        foreach ($products as $oneProduct) {
            if ($oneProduct->id != null)
                array_push($productsIds, $oneProduct->id);
        }

        return $productsIds;
    }

    public function buyIfCan($user) {
        $AllCount = $this->getSumCountProductsInBasketIsAuth($user);
        if ($AllCount < $user['count']) {
            $user['count'] = $user['count'] - $AllCount;
            $this->Users->save($user);


            return $this->pay($user);
        }
        return false;
    }

    public function pay($user) {
        $order = $this->getOrderForThisUserNotPaid($user);
        if ($order) {
            $order->is_paid = 1;

            $this->Orders->save($order);
            return true;
        } else {
            return false;
        }
    }

    public function initialize(array $config) {
        parent::initialize($config);

        $this->table('products');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsToMany('Orders', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'order_id',
            'joinTable' => 'orders_products'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create');

        $validator
                ->allowEmpty('name');

        $validator
                ->add('price', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('price');

        $validator
                ->allowEmpty('descpription');

        return $validator;
    }


    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }

}
