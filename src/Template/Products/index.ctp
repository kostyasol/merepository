<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <?php $AllCount = 0.0;
        if ($IsAdmin) {
            ?>
            <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?></li>
        <?php } ?>

        <?php
        if (isset($prodId_Count_Array)) {
            foreach ($prodId_Count_Array as $id => $count) {
                $currentPrintProduct;
                foreach ($productsForBasket as $oneProduct) {
                    if ($oneProduct->id == $id) {

                        $currentPrintProduct = $oneProduct;
                        break;
                    }
                }
                $AllCount+=$currentPrintProduct->price * $count;
                ?>
                <td> <?php echo ($currentPrintProduct->name) ?></td></br>
                <td><?php echo ($currentPrintProduct->price) ?></td></br>
                <td><?php echo ($count) ?></td></br>
                <td><?php echo ($currentPrintProduct->descpription) ?></td></br>
                <?php echo $this->Html->link('Delete', ['action' => 'deleteFromBasket', $currentPrintProduct->id]) ?></br>

            <?php }if ($AllCount > 0) {
                echo 'All count: ' . $AllCount;
                ?>
                <?php echo $this->Html->link('buy', ['action' => 'buy']) ?></br>
            <?php } ?>
        <?php } ?>
    </ul>
</nav>
<?php
//if ($IsAdmin) {
//    echo 'true';
//} else {
//    echo 'false';
//}
//?>
<?= $this->Flash->render('auth') ?>
<div class="product index large-9 medium-8 columns content">
    <h3><?= __('Products') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('price') ?></th>
                <th><?= $this->Paginator->sort('descpription') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
<?php foreach ($products as $product): ?>
                <tr>
                    <td><?= $this->Number->format($product->id) ?></td>
                    <td><?= h($product->name) ?></td>
                    <td><?= $this->Number->format($product->price) ?></td>
                    <td><?= h($product->descpription) ?></td>
                    <td class="actions">
    <?= $this->Html->link(__('View'), ['action' => 'view', $product->id]) ?>
                        
                        <?php if ($IsAdmin) { ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $product->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?>
                        <?php } else { ?>
<?= $this->Html->link(__('Buy'), ['action' => 'addToBasket', $product->id]) ?>
                        <?php } ?>
                    </td>
                </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
<?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>