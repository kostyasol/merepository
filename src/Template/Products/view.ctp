<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Product'), ['action' => 'index']) ?> </li>
         <?php if($IsAdmin){ ?>
        <li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?> </li>
        <?php } ?>
        
        <?php
        if(isset($prodId_Count_Array)){
        foreach ($prodId_Count_Array as $id => $count) {
            $currentPrintProduct;
            foreach ($productsForBasket as $oneProduct) {
                if ($oneProduct->id == $id) {
                    $currentPrintProduct = $oneProduct;
                    break;
                }
            }
            ?>
            <td> <?php echo ($currentPrintProduct->name) ?></td></br>
            <td><?php echo ($currentPrintProduct->price) ?></td></br>
            <td><?php echo ($count) ?></td></br>
            <td><?php echo ($currentPrintProduct->descpription) ?></td></br>
            <?php echo $this->Html->link('Delete', ['action' => 'index', $currentPrintProduct->id, 'delete']) ?></br>
        <?php } ?>
            <?php } ?>
    </ul>
</nav>
<div class="product view large-9 medium-8 columns content">
    <h3><?= h($product->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($product->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Descpription') ?></th>
            <td><?= h($product->descpription) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($product->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Price') ?></th>
            <td><?= $this->Number->format($product->price) ?></td>
        </tr>
    </table>
</div>
