<?php

namespace App\Controller;

use App\Controller\AppController;
use \App\Model\Entity\Order;
use Cake\Event\Event;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 */
class ProductsController extends AppController {
 public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['display','index','view']);
        
        
    }
    public function isAuthorized($user) {
        $action = $this->request->params['action'];

        if ($this->Auth->user()['id'] == 1) {
            if (in_array($action, ['index', 'view', 'add', 'delete', 'edit'])) {
                return true;
            }
        } else {
            if (in_array($action, ['index', 'view', 'buy','addToBasket','deleteFromBasket'.'buy'])) {
                return true;
            }
        }

        // All other actions require an id.
        if (empty($this->request->params['pass'][0])) {
            return false;
        }
        return parent::isAuthorized($user);
    }

    public function index($id = null, $flag = 'nonDelete') {
        $session = $this->request->session();
         $this->paginate = ['limit'=>10];
        $products = $this->paginate($this->Products);




        if ($this->Auth->isAuthorized()) {
         $order =  $this->Products->getOrderForThisUserNotPaid($this->Auth->user());
            if($order){
            $productsForBasket = $order->products;
            if (isset($productsForBasket)) {
                $nonRepitProducts = $this->Products->generateListStackProductsForAuth($productsForBasket); // ganarate list without repetition
                ksort($nonRepitProducts, SORT_NUMERIC);
                $this->set('prodId_Count_Array', $nonRepitProducts);
                $this->set('productsForBasket', $productsForBasket);
            }
            }
        } else {
            $productsForBasket = $session->read('prod');
            if (isset($productsForBasket)) {
                $nonRepitProducts = $this->Products->generateListStackProductsForNotAuth($productsForBasket); // ganarate list without repetition
                ksort($nonRepitProducts, SORT_NUMERIC);
                $this->set('prodId_Count_Array', $nonRepitProducts);
                $this->set('productsForBasket', $productsForBasket);
            }
        }


        $this->set('IsAdmin', $this->Products->isAdmin($this->Auth->User()['id'])); // define what role is current user
        $this->set(compact('products')); // all product and paganite
        $this->set('_serialize', ['products']);
    }

    public function addToBasket($id = null) {
        if ($id != null) {
            $productInBasket = $this->Products->getProductById($id);
            if ($this->Auth->isAuthorized()) {
                $this->Products->addInBasketProductIsAuth($productInBasket,  $this->Auth->user());
            } else {
                $this->Products->addInBasketProductIsNotAuth($productInBasket, $this->request->session());
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function deleteFromBasket($id = null) {
        if ($id != null) {
            if ($this->Auth->user()!=NULL) {
                
                $this->Products->deleteFromBasketProductIsAuth($id,$this->Auth->user());
            } else {
                $this->Products->deleteFromBasketProductIsNotAuth($id,$this->request->session());
            }
        }


        return $this->redirect(['action' => 'index']);
    }

    public function view($id = null) {
//        $product = $this->Products->get($id, [
//            'contain' => ['Users']
//        ]);

        $product = $this->Products->getProductById($id);
        $session = $this->request->session();

        if ($this->Auth->isAuthorized()) {
         $order =  $this->Products->getOrderForThisUserNotPaid($this->Auth->user());
            if($order){
            $productsForBasket = $order->products;
            if (isset($productsForBasket)) {
                $nonRepitProducts = $this->Products->generateListStackProductsForAuth($productsForBasket); 
                ksort($nonRepitProducts, SORT_NUMERIC);
                $this->set('prodId_Count_Array', $nonRepitProducts);
                $this->set('productsForBasket', $productsForBasket);
            }
            }
        } else {
            $productsForBasket = $session->read('prod');
            if (isset($productsForBasket)) {
                $nonRepitProducts = $this->Products->generateListStackProductsForNotAuth($productsForBasket); 
                ksort($nonRepitProducts, SORT_NUMERIC);
                $this->set('prodId_Count_Array', $nonRepitProducts);
                $this->set('productsForBasket', $productsForBasket);
            }
        }

        $this->set('IsAdmin', $this->Products->isAdmin($this->Auth->User()['id'])); 

        $this->set('product', $product);
        $this->set('_serialize', ['product']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add() {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $users = $this->Products->Users->find('list', ['limit' => 200]);
        $this->set(compact('product', 'users'));
        $this->set('_serialize', ['product']);
    }

    public function buy() {
        if($this->Products->buyIfCan($this->Products->Users->get($this->Auth->user()['id']))){
             $this->Flash->success(__('All products purchased!'));
        }
        else
        {
            $this->Flash->error(__('Insufficient funds in the account!'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function edit($id = null) {
        $product = $this->Products->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->data);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The product could not be saved. Please, try again.'));
            }
        }
        $users = $this->Products->Users->find('list', ['limit' => 200]);
        $this->set(compact('product', 'users'));
        $this->set('_serialize', ['product']);
    }

//    public function beforeFilter(Event $event) {
//        parent::beforeFilter($event);
//        $this->Products->setUserForModel($this->Auth->user());
//    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
